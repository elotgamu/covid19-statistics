import { createSelector } from "redux-bundler";

// set time to retry fetching data to 15 secs
const ERROR_TIME = 15000;
//Set refresh time to 10 min.
const REFRESH_TIME = 600000;
const ActionTypes = {
  FETCH_START: "FETCH_COUNTRIES_START",
  FETCH_ERROR: "FETCH_COUNTRIES_ERROR",
  FETCH_SUCCESS: "FETCH_COUNTRIES_SUCCESS",
  FETCH_DETAIL_SUCCESS: "FETCH_DETAIL_SUCCESS",
};

const countries = {
  name: "countries",
  getReducer: () => {
    const initialState = {
      loading: false,
      lastError: null,
      lastFetch: null,
      data: null,
      error: null,
      selectedCountry: null,
    };

    // Reducer
    return (state = initialState, { type, payload }) => {
      switch (type) {
        case ActionTypes.FETCH_START:
          return {
            ...state,
            loading: true,
          };
        case ActionTypes.FETCH_ERROR:
          return {
            ...state,
            lastError: Date.now(),
            loading: false,
            error: payload,
          };
        case ActionTypes.FETCH_SUCCESS:
          return {
            ...state,
            lastFetch: Date.now(),
            loading: false,
            lastError: null,
            error: null,
            data: payload,
          };
        case ActionTypes.FETCH_DETAIL_SUCCESS:
          return {
            ...state,
            loading: false,
            lastError: null,
            error: null,
            selectedCountry: payload,
          };
        default:
          return state;
      }
    };
  },

  //Selectors
  selectCountriesDataRaw: (state) => state.countries,
  selectCountriesData: (state) => state.countries.data,
  selectApiError: (state) => state.countries.error,
  selectIsLoading: createSelector(
    "selectCountriesDataRaw",
    (countries) => countries.loading
  ),
  selectCountryList: createSelector("selectCountriesData", (countries) => {
    return countries
      ? countries.filter((country) => country.continent !== country.country)
      : [];
  }),
  selectCurrentCountry: (state) => state.countries.selectedCountry,
  selectCountriesErrorMessage: createSelector(
    "selectCountriesDataRaw",
    (countriesDataRaw) =>
      countriesDataRaw.error && countriesDataRaw.error.message
        ? countriesDataRaw.error.message
        : null
  ),

  //Action Creators

  //Fetch country statistic list
  doFetchCountriesStatistics:
    () =>
    ({ dispatch, apiFetch }) => {
      dispatch({ type: ActionTypes.FETCH_START });
      apiFetch({
        endpoint: "statistics",
      })
        .then((payload) => {
          dispatch({
            type: ActionTypes.FETCH_SUCCESS,
            payload: payload.response,
          });
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.FETCH_ERROR, payload: error });
        });
    },

  //Fetch country details
  doFetchSelectedCountry:
    (country) =>
    ({ dispatch, apiFetch }) => {
      dispatch({ type: ActionTypes.FETCH_START });
      apiFetch({
        endpoint: "statistics",
        params: {
          country,
        },
        redirectOnNotFound: true,
      })
        .then((payload) => {
          dispatch({
            type: ActionTypes.FETCH_DETAIL_SUCCESS,
            payload: payload.response[0],
          });
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.FETCH_ERROR, payload: error });
        });
    },

  //Reactors
  // We refetch data each
  // 10 min (REFRESH_TIME)
  // or each 15 sec (ERROR_TIME) if api errors
  // This  "reactor" pattern  react to the
  // application state to dispatch other actions,
  // recover from errors or tolerate terrible network conditions.

  //Reactor to dispatch the
  // fetch of country statistics
  reactShouldFetCountriesData: createSelector(
    "selectCountriesDataRaw",
    "selectAppTime",
    (countries, appTime) => {
      if (countries.loading) {
        return null;
      }
      let shouldFetch = false;

      if (!countries.data && !countries.lastError) {
        shouldFetch = true;
      } else if (countries.lastError) {
        const timePassed = appTime - countries.lastError;
        if (timePassed > ERROR_TIME) {
          shouldFetch = true;
        }
      } else if (countries.lastFetch) {
        const timePassed = appTime - countries.lastFetch;
        if (timePassed > REFRESH_TIME) {
          shouldFetch = true;
        }
      }

      if (shouldFetch) {
        return { actionCreator: "doFetchCountriesStatistics" };
      }
    }
  ),

  // reactor to fetch the country details
  // based on query parameters
  reactShouldFetchCountryDetails: createSelector(
    "selectCountriesDataRaw",
    "selectAppTime",
    "selectRouteParams",
    (countries, appTime, routeParams) => {
      if (countries.loading || !routeParams.countryName) {
        return null;
      }

      let shouldFetch = false;

      if (
        (!countries.selectedCountry ||
          countries.selectedCountry.country !== routeParams.countryName) &&
        !countries.lastError
      ) {
        shouldFetch = true;
      } else if (countries.lastError) {
        const timePassed = appTime - countries.lastError;
        if (timePassed > ERROR_TIME) {
          shouldFetch = true;
        }
      } else if (countries.lastFetch) {
        const timePassed = appTime - countries.lastFetch;
        if (timePassed > REFRESH_TIME) {
          shouldFetch = true;
        }
      }

      if (shouldFetch) {
        return {
          actionCreator: "doFetchSelectedCountry",
          args: [routeParams.countryName],
        };
      }
    }
  ),
};

export default countries;
