import { createRouteBundle } from "redux-bundler";

import HomePage from "../pages/HomePage";
import DetailPage from "../pages/DetailPage";
import NotFound from "../pages/NotFound";

export default createRouteBundle({
  "/": {
    title: "Home Page",
    component: HomePage,
  },
  "/country/:countryName": {
    title: "Country Details",
    component: DetailPage,
  },
  "*": {
    title: "Page Not Found",
    component: NotFound,
  },
});
