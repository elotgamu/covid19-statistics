import axios from "axios";
import { getApiError } from "../../utils";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const headers = {
  "x-rapidapi-host": process.env.REACT_APP_RAPID_API_HOST,
  "x-rapidapi-key": process.env.REACT_APP_RAPID_API_KEY,
};

const api = {
  name: "apiv1",
  // note that the store gets passed in here:
  getExtraArgs: (store) => ({
    /**
     * Call to Axios instance
     * @param   {Object} config  Config object for the axios instance
     *          {string}  method
     *          {string}  url
     *          {Object}  param              Query params
     *          {Boolean} redirectOnNotFound Redirect to not found component
     *          {Boolean} alertOnFail        Throw general alert on BAD REQUEST
     * @return  {Promise}  Axios instance
     */
    apiFetch: (config) =>
      axios({
        ...config,
        method: "GET",
        baseURL: BASE_URL,
        url: `/${config.endpoint}`,
        params: config.params || {},
        headers,
      })
        .then((res) => {
          //Api does not return 404
          // it return 0 as results
          if (!res.data.results && config.redirectOnNotFound) {
            store.doUpdateUrl("/not-found");
          }
          return res.data;
        })
        .catch((err) => {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          if (err.response) {
            const apiHandledResponse = getApiError(err.response);

            // Create a route for handling 404 errors
            if (err.response.status === 404 && config.redirectOnNotFound) {
              store.doUpdateUrl("/not-found");
            }

            throw apiHandledResponse;
          }

          // Otherwise return the error message from axios instance
          // Here we need the error message only but lets keep the error object format { message: err.message}
          const errorInstance = { message: err.message };
          throw errorInstance;
        }),
  }),
};

export default api;
