import { composeBundles } from "redux-bundler";

/** Routing and fetch Data */
import api from "./lib/api";
import appIdle from "./lib/app-idle";
import routes from "./routes";

//custom modules
import countries from "./countries";
import search from "./search";

export default composeBundles(
  appIdle({
    idleTimeout: process.env.NODE_ENV === "production" ? 1000 : 15000,
  }),
  api,
  routes,
  countries,
  search
);
