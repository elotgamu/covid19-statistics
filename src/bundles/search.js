import { createSelector } from "redux-bundler";

import { filterByCountry } from "../utils";
const ActionTypes = {
  SET_SEARCH: "SET_SEARCH",
};

const search = {
  name: "search",
  getReducer: () => {
    const initialState = {
      loading: false,
      lastError: null,
      lastFetch: null,
      data: "",
      error: null,
    };

    return (state = initialState, { type, payload }) => {
      switch (type) {
        case ActionTypes.SET_SEARCH:
          return {
            ...state,
            data: payload,
          };

        default:
          return state;
      }
    };
  },

  //Selectors
  selectSearchData: (state) => state.search.data,
  selectSearchResults: createSelector(
    "selectCountryList",
    "selectSearchData",
    (countryList, searchData) => {
      if (!searchData) {
        return countryList;
      }

      return filterByCountry(countryList, searchData);
    }
  ),

  //action creators
  doSearchByCountry:
    (searchTerm) =>
    ({ dispatch }) => {
      dispatch({ type: ActionTypes.SET_SEARCH, payload: searchTerm });
    },
};

export default search;
