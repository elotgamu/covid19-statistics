import * as React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { useConnect } from "redux-bundler-hook";
import AppSearch from "../../components/AppSearch";

jest.mock("redux-bundler-hook", () => ({
  ...jest.requireActual("redux-bundler-hook"),
  useConnect: jest.fn(),
}));

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

describe("AppSearch component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should render Simple Search", () => {
    const doSearchMock = jest.fn();
    useConnect.mockImplementationOnce(() => ({
      searchData: "",
      route: { title: "Home Page" },
      searchResults: [],
      doSearchByCountry: doSearchMock,
      doUpdateUrl: jest.fn(),
    }));
    render(<AppSearch />);
    const simpleSearch = screen.getByPlaceholderText("Search for country");
    expect(simpleSearch).toBeInTheDocument();
    expect(simpleSearch).toHaveValue("");
    userEvent.type(simpleSearch, "Nicaragua");
    expect(doSearchMock).toHaveBeenCalled();
  });

  it("should render AutoComplete Search", () => {
    const doSearchMock = jest.fn();
    useConnect.mockImplementationOnce(() => ({
      searchData: "",
      route: { title: "" },
      searchResults: [],
      doSearchByCountry: doSearchMock,
      doUpdateUrl: jest.fn(),
    }));
    render(<AppSearch />);
    const autoComplete = screen.getByRole("combobox");
    expect(autoComplete).toBeInTheDocument();
    expect(autoComplete).toHaveValue("");
    userEvent.type(autoComplete, "Nicaragua");
    expect(doSearchMock).toHaveBeenCalled();
  });
});
