import * as React from "react";
import { render, screen } from "@testing-library/react";
import { useConnect } from "redux-bundler-hook";
import HomePage from "../../pages/HomePage";

import { someCountries } from "../../mockedData/countries";

jest.mock("redux-bundler-hook", () => ({
  ...jest.requireActual("redux-bundler-hook"),
  useConnect: jest.fn(),
}));

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

describe("Homepage component", () => {
  afterEach(() => {
    useConnect.mockClear();
  });
  it("should render homepage with no results", () => {
    useConnect.mockImplementationOnce(() => ({
      IsLoading: false,
      countryList: [],
    }));
    render(<HomePage />);
    expect(screen.getByText("Countries Statistics")).toBeInTheDocument();
    expect(screen.getByText(/No results found/i)).toBeInTheDocument();
  });

  it("should render homepage with result", () => {
    useConnect.mockImplementationOnce(() => ({
      searchResults: someCountries,
      isLoading: false,
    }));
    render(<HomePage />);
    expect(screen.getByText("Countries Statistics")).toBeInTheDocument();
    expect(screen.queryByText(/No results found/i)).not.toBeInTheDocument();
    expect(screen.getByText("Montserrat")).toBeInTheDocument();
    expect(screen.getByText("Nicaragua")).toBeInTheDocument();
    expect(screen.getAllByText("Oceania").length).toBe(2);
  });

  it("should render loading state", () => {
    useConnect.mockImplementationOnce(() => ({
      searchResults: [],
      isLoading: true,
    }));
    render(<HomePage />);
    expect(screen.getByText(/loading/i)).toBeInTheDocument();
  });
});
