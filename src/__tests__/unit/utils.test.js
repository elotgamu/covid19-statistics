import { filterByCountry } from "../../utils";

describe("filterByCountry", () => {
  it("should return no results on empty list", () => {
    const list = filterByCountry();
    expect(list.length).toBe(0);
  });
});
