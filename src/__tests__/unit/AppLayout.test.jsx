import * as React from "react";
import { render, screen } from "@testing-library/react";
import { useConnect } from "redux-bundler-hook";

import AppLayout from "../../components/AppLayout";
import NotFound from "../../pages/NotFound";

jest.mock("redux-bundler-hook", () => ({
  ...jest.requireActual("redux-bundler-hook"),
  useConnect: jest.fn(),
}));

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

describe("AppLayout", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should render correct component", () => {
    useConnect.mockImplementationOnce(() => ({
      route: { component: NotFound, title: "Page Not Found" },
      doUpdateUrl: jest.fn(),
    }));
    render(<AppLayout />);
    expect(screen.getByText(/Content not found/i)).toBeInTheDocument();
  });
});
