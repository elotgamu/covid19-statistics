import * as React from "react";
import { render, screen } from "@testing-library/react";
import NotFound from "../../pages/NotFound";

describe("Detail Page component", () => {
  it("should render detail page with no name as param", () => {
    render(<NotFound />);
    expect(screen.getByText("Content not found!")).toBeInTheDocument();
  });
});
