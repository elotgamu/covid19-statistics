import * as React from "react";
import { render, screen } from "@testing-library/react";

import AppTable from "../../components/AppTable";

// example data to render
import { someCountries } from "../../mockedData/countries";

// needed because antd makes
// jest to fail if not included
//the below snippet
// https://stackoverflow.com/a/64872224
global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

describe("AppTable component", () => {
  it("should render `No results` message", () => {
    render(<AppTable countries={[]} />);
    expect(screen.getByText(/No results found/i)).toBeInTheDocument();
  });

  it("should render results", () => {
    render(<AppTable countries={someCountries} />);
    expect(screen.queryByText(/No results found/i)).not.toBeInTheDocument();
    expect(screen.getByText("Montserrat")).toBeInTheDocument();
    expect(screen.getByText("Nicaragua")).toBeInTheDocument();
    expect(screen.getAllByText("Oceania").length).toBe(2);
  });
});
