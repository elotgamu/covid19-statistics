import * as React from "react";
import { render, screen } from "@testing-library/react";
import { useConnect } from "redux-bundler-hook";

import DetailPage from "../../pages/DetailPage";
import { countryData } from "../../mockedData/countries";

jest.mock("redux-bundler-hook", () => ({
  ...jest.requireActual("redux-bundler-hook"),
  useConnect: jest.fn(),
}));

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

describe("Detail Page component", () => {
  afterEach(() => {
    useConnect.mockClear();
  });

  it("should render detail page country name", () => {
    useConnect.mockImplementationOnce(() => ({
      routeParams: { countryName: "Nicaragua" },
      currentCountry: countryData,
    }));
    render(<DetailPage />);
    expect(screen.getByText("Nicaragua")).toBeInTheDocument();
    expect(screen.getByText(/Continent: North-America/i)).toBeInTheDocument();
  });
});
