import * as React from "react";
import userEvent from "@testing-library/user-event";
import { act, render, screen, waitFor } from "@testing-library/react";

// MSW mock
import { server } from "../../mocks/server";

import App from "../../App";

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

window.scrollTo = jest.fn();

// setup api calls
beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("App integration test", () => {
  it("should render list of countries", async () => {
    await act(async () => render(<App />));
    await waitFor(() =>
      expect(screen.queryByText(/loading/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByText("No results found")).not.toBeInTheDocument()
    );
    const searchInput = screen.getByLabelText("Search");
    const title = screen.getByText("Countries Statistics");
    expect(searchInput).toBeInTheDocument();
    expect(searchInput).toHaveValue("");
    expect(title).toBeInTheDocument();
    expect(screen.getAllByText("Oceania").length).toBe(2);
    expect(screen.getByText("Nicaragua")).toBeInTheDocument();
  });

  it("should filter existing country", async () => {
    await act(async () => render(<App />));
    await waitFor(() =>
      expect(screen.queryByText(/loading/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByText("No results found")).not.toBeInTheDocument()
    );

    expect(screen.queryByText("Palau")).toBeInTheDocument();
    expect(screen.queryByText("Micronesia")).toBeInTheDocument();
    const searchInput = screen.getByLabelText("Search");
    expect(searchInput).toHaveValue("");
    userEvent.type(searchInput, "Mont");
    expect(
      screen.getByRole("link", { name: "Montserrat" })
    ).toBeInTheDocument();
    expect(screen.queryByText("Palau")).not.toBeInTheDocument();
    expect(screen.queryByText("Micronesia")).not.toBeInTheDocument();
  });

  it("should allow user to go to detail view", async () => {
    await act(async () => render(<App />));
    await waitFor(() =>
      expect(screen.queryByText(/loading/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByText("No results found")).not.toBeInTheDocument()
    );

    const searchInput = screen.getByLabelText("Search");
    userEvent.type(searchInput, "Nica");

    const linkToCountry = screen.getByText("Nicaragua");
    userEvent.click(linkToCountry);

    await waitFor(() =>
      expect(
        screen.queryByRole("heading", { name: "Nicaragua" })
      ).toBeInTheDocument()
    );

    // Elements for the details view should be in the page
    expect(screen.getByText(/Population: 6714718/i)).toBeInTheDocument();
    expect(screen.getByText("Cases")).toBeInTheDocument();

    // Search component should keep the search criteria
    const searchComponent = screen.getByLabelText("Search");
    expect(searchComponent).toBeInTheDocument();
    expect(searchComponent).toHaveValue("Nica");
  });
});
