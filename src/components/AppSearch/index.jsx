import * as React from "react";

import { useConnect } from "redux-bundler-hook";
import { Input, AutoComplete } from "antd";
import styles from "./styles.module.css";

const { Search } = Input;

const AppSearch = () => {
  const { searchData, route, searchResults, doSearchByCountry, doUpdateUrl } =
    useConnect(
      "selectSearchData",
      "selectRoute",
      "selectSearchResults",
      "doSearchByCountry",
      "doUpdateUrl"
    );

  return (
    <div>
      <label htmlFor="search" className={styles.label}>
        Search
      </label>
      {route.title === "Home Page" ? (
        <Search
          id="search"
          placeholder="Search for country"
          allowClear
          onChange={(e) => doSearchByCountry(e.target.value)}
          value={searchData}
        />
      ) : (
        <AutoComplete
          id="search"
          value={searchData}
          options={searchResults.map((r) => ({
            label: r.country,
            value: r.country,
          }))}
          style={{ width: 200 }}
          placeholder="Search for country"
          onChange={(value) => {
            doSearchByCountry(value);
          }}
          onSelect={(value) => doUpdateUrl(`/country/${value}`)}
          allowClear
        />
      )}
    </div>
  );
};

export default AppSearch;
