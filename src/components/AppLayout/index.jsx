import * as React from "react";

import { getNavHelper } from "internal-nav-helper";
import { useConnect } from "redux-bundler-hook";

import { Divider, Layout, Row, Col } from "antd";

import AppSearch from "../AppSearch";

import styles from "./styles.module.css";

const { Header, Content, Footer } = Layout;

const AppLayout = () => {
  const { route, doUpdateUrl } = useConnect("selectRoute", "doUpdateUrl");
  const Page = route.component;

  return (
    <div onClick={getNavHelper(doUpdateUrl)}>
      <Layout>
        <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
          <div className={styles.logo}>
            <a title="Home" href="/">
              <img
                src="https://rapidapi.com/cdn/images?url=https://rapidapi-prod-collections.s3.amazonaws.com/1934d868-b8b9-40bf-a3a0-a978f007f89d.png"
                alt="Covid19 Fast Api logo"
              />
              {/* Covid19 API */}
            </a>
          </div>
        </Header>

        <Content className={styles.siteLayout}>
          <div className={styles.siteLayoutContent}>
            {route.title !== "Page Not Found" ? (
              <Row>
                <Col>
                  <div className={styles.searchContainer}>
                    <AppSearch />
                  </div>
                </Col>
                <Divider />
              </Row>
            ) : null}

            {/* The Page to display */}
            <Page />
          </div>
        </Content>

        <Footer className={styles.footer}>Covid19 Statistics API</Footer>
      </Layout>
      ,
    </div>
  );
};

export default AppLayout;
