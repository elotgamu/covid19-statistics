import * as React from "react";
import { Row, Col, Card, Statistic, Skeleton } from "antd";
import AppCard from "../AppCard";

import "./styles.css";

const AppDetails = ({ data, loading }) => {
  return (
    <div>
      {data ? (
        <div>
          <h2>{data.country}</h2>
          <h4>Continent: {data.continent || "No Data"}</h4>
          <p>Population: {data.population || "No Data"}</p>

          <Row gutter={24}>
            <Col xs={12} md={6} style={{ marginBottom: 24 }}>
              <AppCard data={data.cases} title="Cases" loading={loading} />
            </Col>
            <Col xs={12} md={6} style={{ marginBottom: 24 }}>
              <AppCard data={data.deaths} title="Deaths" loading={loading} />
            </Col>
            <Col xs={12} md={6} style={{ marginBottom: 24 }}>
              {/* Card for recovered is a special
          case since data lives under cases */}
              <Card loading={loading}>
                <Statistic
                  className="statistics"
                  title="Recovered"
                  value={data.cases.recovered}
                  valueStyle={{ color: "#52c41a" }}
                ></Statistic>
              </Card>
            </Col>
            <Col xs={12} md={6} style={{ marginBottom: 24 }}>
              <AppCard data={data.tests} title="Tests" loading={loading} />
            </Col>
          </Row>
        </div>
      ) : (
        <Skeleton />
      )}
    </div>
  );
};

export default AppDetails;
