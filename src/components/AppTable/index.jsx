import * as React from "react";
import sortBy from "lodash/sortBy";
import dayjs from "dayjs";

import { Table } from "antd";

import styles from "./styles.module.css";

/**
 * @type {React.FC}
 * @param {Object} props
 * @param {Array}  props.countries
 *
 */
const AppTable = ({ countries = [] }) => {
  const columns = [
    {
      title: "Country",
      dataIndex: "country",
      render: (text) => (
        <a
          className={styles.countryLink}
          title={`${text}`}
          href={`/country/${text}`}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Continent",
      dataIndex: "continent",
    },
    {
      title: "Population",
      dataIndex: "population",
      render: (text) => (
        <div className={styles.population}>
          {text ? text.toLocaleString() : 0}
        </div>
      ),
      responsive: ["sm"],
    },
    {
      title: "Total Cases",
      render: (record) => (
        <div className={styles.cases}>
          {record.cases.total ? record.cases.total.toLocaleString() : 0}
        </div>
      ),
      responsive: ["sm"],
    },
    {
      title: "Last Updated",
      render: (record) => dayjs(record.time).format("MM/DD/YYYY HH:MM"),
      key: "time",
      responsive: ["sm"],
    },
  ];

  const data = countries.length
    ? sortBy(countries, "continent").map((country) => country)
    : [];

  return (
    <Table
      columns={columns}
      dataSource={data}
      rowKey="country"
      pagination={{ pageSize: 50, position: ["bottomRight"] }}
      scroll={{ y: 400 }}
      hasData={!!data.length}
      locale={{ emptyText: <p>No results found</p> }}
    />
  );
};

export default AppTable;
