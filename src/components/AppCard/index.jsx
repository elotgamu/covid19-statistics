import * as React from "react";
import { Card, Statistic } from "antd";

const type = {
  Deaths: "#cf1322",
  Cases: "#fa8c16",
  Test: "",
};

const AppCard = ({ data, title, loading }) => {
  return (
    <Card loading={loading}>
      <Statistic
        className="statistics"
        title={title}
        value={data.total ? data.total : "No data"}
        valueStyle={{ color: type[title] }}
      ></Statistic>
    </Card>
  );
};

export default AppCard;
