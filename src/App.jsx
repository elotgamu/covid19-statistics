import * as React from "react";
import { ReduxBundlerProvider } from "redux-bundler-hook";

import getStore from "./bundles";

import AppLayout from "./components/AppLayout";

const App = () => {
  return (
    <ReduxBundlerProvider store={getStore()}>
      <AppLayout />
    </ReduxBundlerProvider>
  );
};

export default App;
