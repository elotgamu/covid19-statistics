import escapeRegExp from "lodash/escapeRegExp";
import filter from "lodash/filter";

export const getApproxTime = (time) => Math.max(0, Math.round(time));

/**
 * Build the response object from server in a standard way
 * to have message, statusCode, errors (if any) properties
 * @param {Object} err   Axios err.response object
 * @return {Object}
 */
export const getApiError = (err) => {
  const {
    data: { message },
    status,
  } = err;

  return { statusCode: status, message };
};

/**
 *
 * @param {Array}  data       'The whole array of data
 * @param {string} searchData 'The search term to filter
 * @return {Array}
 */
export const filterByCountry = (data = [], searchTerm) => {
  if (!searchTerm) {
    return data;
  }

  const re = new RegExp(escapeRegExp(searchTerm), "i");
  const isMatch = (result) => re.test(result.country);
  const filteredResults = filter(data, isMatch);
  return filteredResults;
};
