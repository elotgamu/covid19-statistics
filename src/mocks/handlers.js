import { rest } from "msw";
import {
  countryStatisticsResponse,
  countryDetailResponse,
  noFoundCountryResponse,
} from "../mockedData/countries";

/**
 * @typedef {import ('msw').RequestHandler} Request
 * @typedef {import ('msw').ResponseFunction} Response
 * @typedef {import ('msw').RestContext} Context
 */

export const handlers = [
  /**
   * @param {string} path
   * @param {Function} handler
   * @param {Request} handler.req
   * @param {Response} handler.res
   * @param {Context}  handler.ctx
   */
  rest.get("/statistics", (req, res, ctx) => {
    const country = req.url.searchParams.get("country");

    if (!country) {
      return res(ctx.status(200), ctx.json(countryStatisticsResponse));
    }

    if (country === "Nicaragua") {
      return res(ctx.status(200), ctx.json(countryDetailResponse));
    }

    return res(ctx.status(200), ctx.json(noFoundCountryResponse));
  }),
];
