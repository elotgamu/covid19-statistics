export const someCountries = [
  {
    continent: "Oceania",
    country: "Palau",
    population: 18193,
    cases: {
      new: null,
      active: 2,
      critical: null,
      recovered: null,
      "1M_pop": "110",
      total: 2,
    },
    deaths: {
      new: null,
      "1M_pop": null,
      total: null,
    },
    tests: {
      "1M_pop": "451877",
      total: 8221,
    },
    day: "2021-08-28",
    time: "2021-08-28T20:30:04+00:00",
  },
  {
    continent: "Oceania",
    country: "Micronesia",
    population: 115715,
    cases: {
      new: null,
      active: 0,
      critical: null,
      recovered: 1,
      "1M_pop": "9",
      total: 1,
    },
    deaths: {
      new: null,
      "1M_pop": null,
      total: null,
    },
    tests: {
      "1M_pop": null,
      total: null,
    },
    day: "2021-08-28",
    time: "2021-08-28T20:30:04+00:00",
  },
  {
    continent: "North-America",
    country: "Montserrat",
    population: 4995,
    cases: {
      new: "+1",
      active: 5,
      critical: null,
      recovered: 20,
      "1M_pop": "5205",
      total: 26,
    },
    deaths: {
      new: null,
      "1M_pop": "200",
      total: 1,
    },
    tests: {
      "1M_pop": "281882",
      total: 1408,
    },
    day: "2021-08-28",
    time: "2021-08-28T20:30:04+00:00",
  },
  {
    continent: "North-America",
    country: "Nicaragua",
    population: 6714718,
    cases: {
      new: "+495",
      active: 6743,
      critical: null,
      recovered: 4225,
      "1M_pop": "1663",
      total: 11167,
    },
    deaths: {
      new: "+1",
      "1M_pop": "30",
      total: 199,
    },
    tests: {
      "1M_pop": null,
      total: null,
    },
    day: "2021-08-29",
    time: "2021-08-29T19:30:05+00:00",
  },
];

export const countryData = {
  continent: "North-America",
  country: "Nicaragua",
  population: 6714718,
  cases: {
    new: "+495",
    active: 6743,
    critical: null,
    recovered: 4225,
    "1M_pop": "1663",
    total: 11167,
  },
  deaths: {
    new: "+1",
    "1M_pop": "30",
    total: 199,
  },
  tests: {
    "1M_pop": null,
    total: null,
  },
  day: "2021-08-29",
  time: "2021-08-29T19:30:05+00:00",
};

export const countryStatisticsResponse = {
  get: "statistics",
  parameters: [],
  errors: [],
  results: 235,
  response: someCountries,
};

export const countryDetailResponse = {
  get: "statistics",
  parameters: {
    country: "Nicaragua",
  },
  errors: [],
  results: 1,
  response: [countryData],
};

export const noFoundCountryResponse = {
  get: "statistics",
  parameters: {
    country: "test",
  },
  errors: [],
  results: 0,
  response: [],
};
