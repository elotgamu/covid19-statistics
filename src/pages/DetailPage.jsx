import * as React from "react";
import { useConnect } from "redux-bundler-hook";

import AppDetails from "../components/AppDetails";

const DetailPage = () => {
  const { currentCountry, isLoading } = useConnect(
    "selectCurrentCountry",
    "selectIsLoading"
  );

  return (
    <div>
      <div>
        <AppDetails data={currentCountry} loading={isLoading} />
      </div>
    </div>
  );
};

export default DetailPage;
