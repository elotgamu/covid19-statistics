import * as React from "react";
import { WarningOutlined } from "@ant-design/icons";

const NotFound = () => {
  return (
    <div style={{ textAlign: "center", paddingTop: 50 }}>
      <WarningOutlined style={{ fontSize: "50px", color: "#d46b08" }} />
      <h1>Content not found!</h1>
      <a title="Go back to home" href="/">
        Go back to home
      </a>
    </div>
  );
};

export default NotFound;
