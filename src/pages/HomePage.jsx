import * as React from "react";
import { useConnect } from "redux-bundler-hook";

import { Spin } from "antd";

import AppTable from "../components/AppTable";

const HomePage = () => {
  const { searchResults, isLoading } = useConnect(
    "selectSearchResults",
    "selectIsLoading"
  );

  if (isLoading) {
    return (
      <div style={{ width: "100%", textAlign: "center" }}>
        <Spin tip="loading" size="large" />
      </div>
    );
  }

  return (
    <div>
      <h1>Countries Statistics</h1>
      <AppTable countries={searchResults} />
    </div>
  );
};

export default HomePage;
