# Covid19 Country Statistics

[![Netlify Status](https://api.netlify.com/api/v1/badges/92e64dc4-a1ed-4ef2-8b59-c95a3165d09a/deploy-status)](https://app.netlify.com/sites/boring-pasteur-570fc0/deploys)

Deployed at: [https://boring-pasteur-570fc0.netlify.app/](https://boring-pasteur-570fc0.netlify.app/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Packages used:

1. [redux-bundler](https://github.com/HenrikJoreteg/redux-bundler)
2. [ANT Design for React](https://ant.design/docs/react/introduce)
3. [axios](https://github.com/axios/axios)
4. [dayjs](https://github.com/iamkun/dayjs/)

## Environment vars

In order to run this project locally
you will need an api key. Create an account on
[https://rapidapi.com/](https://rapidapi.com/)
to get your API key.

Then you need to create a .env file with the following values

```env
REACT_APP_RAPID_API_KEY=
REACT_APP_RAPID_API_HOST=
REACT_APP_BASE_URL=
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in non interactive mode

### `npm test:coverage`

Launches the test runner and generate coverage report

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
